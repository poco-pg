#!/usr/bin/perl


# - Checks whether spawn() actually creates a session
# - Checks whether the alias is correctly set
# - Tests register() with the error event
# - Tests error event dispatch on failed connect()
# - Tests shutdown() in an idle situation
# - Tests unregister() with the error event
# - Tests whether the calling session terminates after a shutdown
#   (i.e. refcount is correctly decremented)


use Test::More tests => 8;

use POE;
use POE::Component::Pg;

my $ret = POE::Component::Pg->spawn(
  alias => 'pg',
);

# NOTE: we don't check for what it actually returns, as this isn't defined yet.
ok $ret, 'Spawn returns a true value';


my $state = 0;

my $tester = POE::Session->create(inline_states => {
  _start => sub {
    $_[KERNEL]->yield('check_alias');
  },

  check_alias => sub { 
    ok defined $_[KERNEL]->alias_resolve('pg'), 'Session exists and alias is set';
    $_[KERNEL]->post(pg => 'shutdown');
    $_[KERNEL]->yield('check_shutdown');
  },

  check_shutdown => sub {
    ok !defined $_[KERNEL]->alias_resolve('pg'), 'Session has stopped';
    POE::Component::Pg->spawn(alias => 'pg2', dsn => 'dbi:incorrect-name');
    $_[KERNEL]->yield('do_misconnect');
  },

  do_misconnect => sub {
    ok defined $_[KERNEL]->alias_resolve('pg2'), 'Session restart';
    $_[KERNEL]->post(pg2 => register => error => 'misconnect_error', connect => 'misconnect_connect');
    $_[KERNEL]->post(pg2 => 'connect');
    $_[KERNEL]->delay('misconnect_timeout', 5);
  },

  misconnect_error => sub {
    if(!$state++) {
      pass 'Incorrect connection fires error';
      $_[KERNEL]->delay('misconnect_timeout');
      $_[KERNEL]->post(pg2 => unregister => 'error');
      $_[KERNEL]->post(pg2 => 'connect');
      $_[KERNEL]->delay('misconnect_2nd', 1);
    } else {
      fail 'unregister';
    }
  },
  misconnect_connect => sub { fail 'Incorrect connection fires error (connect)' },
  misconnect_timeout => sub { fail 'Incorrect connection fires error (timeout)' },
  misconnect_2nd => sub {
    pass 'unregister';
    $_[KERNEL]->post(pg2 => 'shutdown');
    $_[KERNEL]->yield('check_lastshutdown');
  },

  check_lastshutdown => sub {
    ok !defined $_[KERNEL]->alias_resolve('pg2'), 'Second shutdown';
    $_[KERNEL]->post(watcher => 'shutdown');
  },
});


POE::Session->create(inline_states => {
  _start => sub { $_[KERNEL]->alias_set('watcher') },
  shutdown => sub { $_[KERNEL]->yield('shutdown_done') },
  shutdown_done => sub {
    ok !defined $_[KERNEL]->alias_resolve($tester), 'Refcount reaches zero';
  },
});


$poe_kernel->run();

