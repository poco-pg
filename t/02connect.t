#!/usr/bin/perl

# - Tests connecting to the database
# - Tests that the connect event is dispatched on connection
# - Tests whether shutdown() works while connected
# - Tests that the arguments of connect() overwrite the ones given to spawn()
# - Tests that the arguments given to spawn() are used when connecting

use strict;
use warnings;
use Test::More;
use POE::Component::Pg;
use DBI;
use POE;

my $dbh;
eval {
  $dbh = DBI->connect();
};
if($@) {
  BAIL_OUT("Couldn't connect to PostgreSQL, please set the DBI_DSN, DBI_USER and DBI_PASS environment variables.");
} else {
  plan tests => 7;
}

POE::Component::Pg->spawn(alias => 'pg');


POE::Session->create(inline_states => {
  _start => sub {
    $_[KERNEL]->post(pg => register => connect => 'pg_connect', error => 'pg_error');
    $_[KERNEL]->post(pg => 'connect');
    $_[KERNEL]->delay('connect_timeout', 10);
  },

  pg_connect => sub {
    pass 'Connect';
    $_[KERNEL]->post(pg => 'shutdown');
    $_[KERNEL]->delay('connect_timeout');
    $_[KERNEL]->yield('disconnect');
  },
  pg_error => sub { fail 'Connect (error)' },
  connect_timeout => sub { fail 'Connect (timeout)' },

  disconnect => sub {
    ok !defined $_[KERNEL]->alias_resolve('pg'), 'Connection close';
    POE::Component::Pg->spawn(alias => 'pg', dsn => 'dbi:doesntexists', user => 'wont-ever-work', password => 'some-never-used-password');
    $_[KERNEL]->yield('overwrite');
  },

  overwrite => sub {
    ok defined $_[KERNEL]->alias_resolve('pg'), 'Re-open session';
    $_[KERNEL]->post(pg => register => connect => 'overwrite_connect', error => 'overwrite_error');
    $_[KERNEL]->post(pg => connect => { dsn => $ENV{DBI_DSN}, user => $ENV{DBI_USER}, password => $ENV{DBI_PASS} });
    $_[KERNEL]->delay('overwrite_timeout', 10);
  },
  overwrite_connect => sub {
    pass 'Overwrite';
    $_[KERNEL]->delay('overwrite_timeout');
    $_[KERNEL]->post(pg => 'shutdown');
    $_[KERNEL]->yield('disconnect2');
  },
  overwrite_error => sub { fail 'Overwrite (error)' },
  overwrite_timeout => sub { fail 'Overwrite (timeout)' },

  disconnect2 => sub {
    ok !defined $_[KERNEL]->alias_resolve('pg'), 'Connection close again';
    POE::Component::Pg->spawn(alias => 'pg', dsn => $ENV{DBI_DSN}, user => $ENV{DBI_USER}, password => $ENV{DBI_PASS});
    $ENV{DBI_DSN} = $ENV{DBI_USER} = $ENV{DBI_PASS} = 'this-really-isnt-goint-to-work';
    $_[KERNEL]->yield('spawn');
  },
  spawn => sub {
    ok defined $_[KERNEL]->alias_resolve('pg'), 'Re-open session again';
    $_[KERNEL]->post(pg => register => connect => 'spawn_connect', error => 'spawn_error');
    $_[KERNEL]->post(pg => 'connect');
    $_[KERNEL]->delay('spawn_timeout', 10);
  },
  spawn_connect => sub {
    pass 'Spawn';
    $_[KERNEL]->delay('spawn_timeout');
    $_[KERNEL]->post(pg => 'shutdown');
  },
  spawn_error => sub { fail 'Spawn (error)' },
  spawn_timeout => sub { fail 'Spawn (timeout)' },
});


$poe_kernel->run();
$dbh->disconnect();

