#!/usr/bin/perl

# - Make sure we can load the module
# - Module version is available

use Test::More tests => 3;

use_ok 'POE::Component::Pg';

ok defined($POE::Component::Pg::VERSION), 'Module version exists';
ok length($POE::Component::Pg::VERSION) > 0, 'Module version is non-empty';

