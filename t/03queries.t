#!/usr/bin/perl

use strict;
use warnings;
use Test::More;
use POE::Component::Pg;
use DBI;
use POE;

plan tests => 18;

my $dbh = DBI->connect(undef, undef, undef, { AutoCommit => 1, RaiseError => 1 });
POE::Component::Pg->spawn(alias => 'pg');


# create temporary table for testing (assuming the current user has that right)
eval {
  $dbh->do('CREATE TABLE poco_pg_test (col1 integer, col2 varchar)');
};
BAIL_OUT("Couldn't create table for testing: $@.\nMake sure the SQL user has table creation rights.") if $@;

END {
  $dbh->do('DROP TABLE poco_pg_test');
  $dbh->disconnect();
}



POE::Session->create(inline_states => {
  _start => sub {
    $_[KERNEL]->post(pg => register => connect => 'pg_connect', error => 'pg_error');
    $_[KERNEL]->post(pg => 'connect');
  },
  pg_error => sub { diag "ERROR $_[ARG0]: $_[ARG1]"; fail 'PostgreSQL error'; die },
  pg_connect => sub {
    $_[KERNEL]->post(pg => do => 'INSERT INTO poco_pg_test VALUES(?, ?)', [ $$, 'value1' ], 'insert_done', 'insert-arg');
    $_[KERNEL]->delay('insert_timeout', 10);
  },
  insert_timeout => sub { fail 'Insert (timeout)'; die },
  insert_done => sub {
    $_[KERNEL]->delay('insert_timeout');
    # check DB to see that it has indeed been inserted
    my $q = $dbh->prepare('SELECT col2 FROM poco_pg_test WHERE col1 = ?');
    is $q->execute($$), 1, 'INSERT query';
    is $q->fetchrow_hashref()->{col2}, 'value1', 'INSERTed value';
    is $_[ARG0], 1, 'Insert number argument';
    is $_[ARG1], undef, 'Insert result argument';
    is $_[ARG2], 'insert-arg', 'Insert custom argument';

    # now try to select everything from our table (query without placeholders)
    $_[KERNEL]->post(pg => query => 'SELECT col1, col2 FROM poco_pg_test', undef, 'select_done', 'select-arg');
    $_[KERNEL]->delay('select_timeout', 10);
  },

  select_timeout => sub { fail 'Select (timeout)'; die },
  select_done => sub {
    $_[KERNEL]->delay('select_timeout');
    is $_[ARG0], 1, 'Select number argument';
    is_deeply $_[ARG1], [{ col1 => $$, col2 => 'value1' }], 'Select result argument';
    is $_[ARG2], 'select-arg', 'Select custom argument';

    # now execute a query that is guaranteed to fail
    $_[KERNEL]->post(pg => register => error => 'error_error');
    $_[KERNEL]->post(pg => do => 'SELECT col99 FROM poco_pg_test', undef, 'error_done', 'error-arg');
    $_[KERNEL]->delay('error_timeout', 10);
  },

  error_timeout => sub { fail 'Error (timeout)'; die },
  error_done => sub { fail 'Error (didn\'t fail!?)'; die },
  error_error => sub {
    $_[KERNEL]->delay('error_timeout');
    pass 'Error occured';
    is $_[ARG0], 'do', 'Error 1st argument';
    # let's not rely on the format of argument 2
    is $_[ARG2], 'SELECT col99 FROM poco_pg_test', 'Error 3rd argument';
    is $_[ARG3], undef, 'Error 5th argument';
    is $_[ARG4], $_[SESSION]->ID(), 'Error 4th argument';
    is $_[ARG5], 'error-arg', 'Error 5th argument';

    # lastly, do an UPDATE, but don't use the optional last argument
    $_[KERNEL]->post(pg => register => error => 'pg_error');
    $_[KERNEL]->post(pg => do => 'UPDATE poco_pg_test SET col2 = ? WHERE col1 = ?', [ 'meh', $$ ], 'update_done');
    $_[KERNEL]->delay('update_timeout', 10);
  },

  update_timeout => sub { fail 'Update (timeout)'; die },
  update_done => sub {
    $_[KERNEL]->delay('update_timeout');

    # check DB
    my $q = $dbh->prepare('SELECT col2 FROM poco_pg_test WHERE col1 = ?');
    $q->execute($$);
    is $q->fetchrow_hashref()->{col2}, 'meh', 'UPDATEd value';
    is $_[ARG0], 1, 'Update number argument';
    is $_[ARG1], undef, 'Update result argument';
    is $_[ARG2], undef, 'Update custom argument';

    $_[KERNEL]->post(pg => 'shutdown');
  },
});

$poe_kernel->run();

